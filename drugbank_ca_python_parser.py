#! /usr/bin/python

import os
import xml.etree.cElementTree as ET
import json
#import yaml
import sqlite3

# Files
XML_PATH = os.path.abspath('resources/drugbank.xml')

#XML Tree
try:
    tree = ET.parse(XML_PATH)
    root = tree.getroot()
    print("Parsed Tree with tag: ", root.tag)
    print("Root of the tree has attributes: ", root.attrib)

except (IOError):
    raise 

#Get all the drugs
all_drugs = root.getchildren()
for drug in all_drugs:
    for i in drug.getchildren():
        if i.tag == 'drug':
           print i.tag
